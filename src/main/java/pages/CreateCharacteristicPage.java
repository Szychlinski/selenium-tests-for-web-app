package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

public class CreateCharacteristicPage extends HomePage {
    public CreateCharacteristicPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(id = "ProjectId")
    private WebElement projectName;

    @FindBy(id = "Name")
    private WebElement charactName;

    @FindBy(id = "LowerSpecificationLimit")
    private WebElement lowerSpecLimit;

    @FindBy(id = "UpperSpecificationLimit")
    private WebElement upperSpecLimit;

    @FindBy(xpath = "//span[@data-valmsg-for='UpperSpecificationLimit']")
    private WebElement upperSpecLimitError;

    @FindBy(id = "HistogramBinCount")
    private WebElement histogramBinCount;

    @FindBy(css = "input[type=submit]")
    private WebElement createBtn;

    @FindBy(css = ".x_title h2")
    private WebElement pageTitle;

    @FindBy(linkText = "Back to List")
    private WebElement backToListBtn;

    public CreateCharacteristicPage selectProcess(String processName){
        new Select(projectName).selectByVisibleText(processName);
        return this;
    }

    public CreateCharacteristicPage typeCharacteristicName(String name){
        charactName.clear();
        charactName.sendKeys(name);
        return this;
    }

    public CreateCharacteristicPage typeLowerSpecificationLimit(String lsl){
        lowerSpecLimit.clear();
        lowerSpecLimit.sendKeys(lsl);
        return this;
    }

    public CreateCharacteristicPage typeUpperSpecificationLimit(String usl){
        upperSpecLimit.clear();
        upperSpecLimit.sendKeys(usl);
        return this;
    }

    public CreateCharacteristicPage typeHistogramBinCount(String hbc){
        histogramBinCount.clear();
        histogramBinCount.sendKeys(hbc);
        return this;
    }

    public CharacteristicsPage submitCharacteristic(){
        createBtn.click();
        return new CharacteristicsPage(driver);
    }

    public CreateCharacteristicPage submitCharacteristicWithFailure(){
        createBtn.click();
        return this;
    }

    public CharacteristicsPage backToCharacteristicsPage(){
        backToListBtn.click();
        return new CharacteristicsPage(driver);
    }

    public CreateCharacteristicPage assertCorrectURL(String expUrl){
        Assert.assertEquals(driver.getCurrentUrl(), expUrl, "The URL of Create Characteristics Page" +
                " is incorrect");
        return this;
    }

    public CreateCharacteristicPage assertCorrectPageTitle(){
        Assert.assertTrue(pageTitle.isDisplayed());
        Assert.assertEquals(pageTitle.getText(), "Create characteristic");
        return this;
    }

    public CreateCharacteristicPage assertUpperSpecLimitErrorIsDisplayed(){
        Assert.assertTrue(upperSpecLimitError.isDisplayed());
        Assert.assertEquals(upperSpecLimitError.getText(), "The value '' is invalid.", "The error" +
                " message is incorrect.");
        return this;
    }
}
