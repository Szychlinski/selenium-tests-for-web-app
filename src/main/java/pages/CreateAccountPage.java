package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import java.util.List;

public class CreateAccountPage {
    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#Email")
    private WebElement emailField;

    @FindBy(css = "#Password")
    private WebElement passwordField;

    @FindBy(css = "#ConfirmPassword")
    private WebElement confirmPasswordField;

    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;

    @FindBy(css = "#ConfirmPassword-error")
    private WebElement passwordsDontMatchError;

    @FindBy(css = ".validation-summary-errors li")
    private List<WebElement> passwordValidationErrors;

    public CreateAccountPage typeEmail(String email){
        emailField.clear();
        emailField.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePswd(String pswd){
        passwordField.clear();
        passwordField.sendKeys(pswd);
        return this;
    }

    public CreateAccountPage confirmPswd(String pswd){
        confirmPasswordField.clear();
        confirmPasswordField.sendKeys(pswd);
        return this;
    }

    public HomePage submitRegister(){
        registerBtn.click();
        return new HomePage(driver);
    }

    public CreateAccountPage submitRegisterWithFailure(){
        registerBtn.click();
        return this;
    }

    public CreateAccountPage assertCorrectRegisterURL(String expURL){
        Assert.assertTrue(driver.getCurrentUrl().startsWith(expURL), "The url address of Create Account" +
                " page is incorrect");
        return this;
    }

    public CreateAccountPage assertConfirmPasswordErrorIsDisplayed(){
        Assert.assertTrue(passwordsDontMatchError.isDisplayed(), "There is no information that " +
                "the passwords don't match.");
        return this;
    }

    public CreateAccountPage assertCorrectErrorMessageIsDisplayed(String expMessage){
        Assert.assertTrue(passwordValidationErrors.size()>0, "There is no error message");
        Assert.assertTrue(passwordValidationErrors.stream().anyMatch(e -> e.getText().equals(expMessage)),
                "The displayed message is incorrect");
        return this;
    }
}
