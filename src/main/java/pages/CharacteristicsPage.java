package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class CharacteristicsPage extends HomePage {
    public CharacteristicsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = "a[href$=Characteristics\\/Create]")
    private WebElement addCharBtn;

    @FindBy(css = ".title_left h3")
    private WebElement pageTitle;

    public CreateCharacteristicPage goToCreateCharacteristicPage(){
        addCharBtn.click();
        return new CreateCharacteristicPage(driver);
    }

    public CharacteristicsPage assertCorrectURL(String expUrl){
        Assert.assertEquals(driver.getCurrentUrl(), expUrl, "The URL of Characteristics Page is incorrect");
        return this;
    }

    public CharacteristicsPage assertCorrectPageTitle(){
        Assert.assertTrue(pageTitle.isDisplayed());
        Assert.assertEquals(pageTitle.getText(), "Characteristics");
        return this;
    }

    public CharacteristicsPage assertAddCharBtnIsDisplayed(){
        Assert.assertTrue(addCharBtn.isDisplayed());
        return this;
    }

    public CharacteristicsPage assertAddCharBtnContainsCorrectText(){
        Assert.assertEquals(addCharBtn.getText(), "Add new characteristic", "The button" +
                "doesn't contain the correct text.");
        return this;
    }

    public CharacteristicsPage assertCharacteristicSuccessfullyAdded(String charactName, String expProcessName,
        String expLsl, String expUsl, String expHistogram){
        String characteristicXpath = String.format("//td[text()='%s']/..", charactName);
        WebElement characteristicRow = driver.findElement(By.xpath(characteristicXpath));

        String actProcName = characteristicRow.findElement(By.xpath("./td[1]")).getText();
        String actLsl = characteristicRow.findElement(By.xpath("./td[3]")).getText();
        String actUsl = characteristicRow.findElement(By.xpath("./td[4]")).getText();
        String actHistogram = characteristicRow.findElement(By.xpath("./td[5]")).getText();

        Assert.assertEquals(actProcName, expProcessName);
        Assert.assertEquals(actLsl, expLsl);
        Assert.assertEquals(actUsl, expUsl);
        Assert.assertEquals(actHistogram, expHistogram);
        return this;
    }

    public CharacteristicsPage assertCharacteristicWasNotAdded(String charactName){
        String characteristicXpath = String.format("//td[text()='%s']/..", charactName);
        List<WebElement>  characteristicRow = driver.findElements(By.xpath(characteristicXpath));
        Assert.assertEquals(characteristicRow.size(), 0);
        return this;
    }
}
