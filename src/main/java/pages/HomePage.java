package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class HomePage {
    protected WebDriver driver;

    public HomePage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = ".profile_info h2")
    private WebElement welcomeTxt;

    @FindBy(css = ".side-menu .menu-home")
    private WebElement menuHome;

    @FindBy(css = ".side-menu .menu-workspace")
    private WebElement menuWorkspace;

    @FindBy(xpath = "//a[contains(text(), \"Dashboard\")]")
    private WebElement dashboardPage;

    @FindBy(css = ".child_menu a[href$=Projects]")
    private WebElement processesPage;

    @FindBy(css = ".child_menu a[href$=Characteristics]")
    private WebElement characteristicsPage;


    public HomePage assertWelcomeElementIsDisplayed(){
        Assert.assertTrue(welcomeTxt.isDisplayed(), "Welcome element is not displayede");
        return this;
    }

    public HomePage assertWelcomeElementDisplaysCorrectLogin(String userLogin){
        Assert.assertTrue(welcomeTxt.getText().contains(userLogin),"Welcome element text: '" +
                welcomeTxt.getText() + "' doesn't contain user's login");
        return this;
    }

    public HomePage assertHomePageURL(String expectedURL){
        Assert.assertEquals(driver.getCurrentUrl(), expectedURL, "The URL is incorrect");
        return this;
    }

    private boolean isParentActive(WebElement menuElement){
        WebElement parentElement = menuElement.findElement(By.xpath("./.."));
        return parentElement.getAttribute("class").contains("active");
    }

    public CharacteristicsPage goToCharacteristicsPage(){
        if(!isParentActive(menuWorkspace)){
            menuWorkspace.click();
        }

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.elementToBeClickable(characteristicsPage));
        characteristicsPage.click();
        return new CharacteristicsPage(driver);
    }

    public ProcessesPage goToProcessesPage(){
        if(!isParentActive(menuWorkspace)){
            menuWorkspace.click();
        }

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.elementToBeClickable(processesPage));
        processesPage.click();
        return new ProcessesPage(driver);
    }

    public HomePage goToDashboardPage(){
        if(!isParentActive(menuHome)){
            menuHome.click();
        }

        WebDriverWait wait = new WebDriverWait(driver, 3);
        wait.until(ExpectedConditions.elementToBeClickable(dashboardPage));
        dashboardPage.click();
        return new HomePage(driver);
    }
}
