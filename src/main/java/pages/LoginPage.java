package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import sun.rmi.runtime.Log;

import java.util.ArrayList;
import java.util.List;

public class LoginPage {

    protected WebDriver driver;

    public LoginPage(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(id="Email")
    private WebElement emailTxt;

    @FindBy(id="Password")
    private WebElement pswdTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement lgnBtn;

    @FindBy(linkText = "Register as a new user?")
    private WebElement regNewUsrLink;

    @FindBy(css = ".validation-summary-errors li")
    private List<WebElement> formErrorsList;

    @FindBy(css = "#Password-error")
    private WebElement missingPasswordError;

    public LoginPage typeEmail(String email){
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public LoginPage typePassword(String password){
        pswdTxt.clear();
        pswdTxt.sendKeys(password);
        return this;
    }

    public HomePage submitLogin(){
        lgnBtn.click();
        return new HomePage(driver);
    }

    public LoginPage submitLoginWithFailure(){
        lgnBtn.click();
        return this;
    }

    public CreateAccountPage goToCreateNewUserPage(){
        regNewUsrLink.click();
        return new CreateAccountPage(driver);
    }

    public HomePage loginAs(String email, String password){
        typeEmail(email);
        typePassword(password);
        return submitLogin();
    }

    public LoginPage assertPswdErrorIsDisplayed(){
        Assert.assertTrue(missingPasswordError.isDisplayed(), "The notification about missing password is" +
                " not displayed");
        return this;
    }

    public LoginPage assertPswdErrorContainsCorrectMessage(String expMessage){
        Assert.assertEquals(missingPasswordError.getText(), expMessage);
        return this;
    }

}
