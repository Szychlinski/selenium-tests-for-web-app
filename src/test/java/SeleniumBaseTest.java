import config.Config;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.util.concurrent.TimeUnit;

public class SeleniumBaseTest {
    protected WebDriver driver;
    protected Config config = new Config();

    @BeforeMethod
    public void baseBeforeMethod(){
        System.setProperty("webdriver.chrome.driver", config.getChromeDriverPath());
        driver = new ChromeDriver();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(config.getApplicationUrl());
    }

    @AfterMethod
    public void baseAfterMethod(){ driver.close(); }

    public String generateNewPassword(int specialChars, int upperCase, int lowerCase, int numbers){
        return RandomStringUtils.random(specialChars, 33, 47, false, false) +
                RandomStringUtils.random(upperCase, 65, 90, true, false) +
                RandomStringUtils.random(lowerCase, 97, 122, true, false) +
                RandomStringUtils.randomNumeric(numbers);
    }
}
