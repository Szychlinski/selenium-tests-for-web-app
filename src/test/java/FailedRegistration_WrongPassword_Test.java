import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class FailedRegistration_WrongPassword_Test extends SeleniumBaseTest {

    @DataProvider
    public static Object[][] getWrongPasswords(){
        return new Object[][]{
                {"test2#", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Test#$", "Passwords must have at least one digit ('0'-'9')."},
                {"Test123", "Passwords must have at least one non alphanumeric character."}
        };
    }

    @Test(dataProvider = "getWrongPasswords")
    public void failedRegistrationWrongPassword(String wrongPassword, String expMessage){
        String generateEmail = RandomStringUtils.randomAlphanumeric(10) + "@test.com";

        new LoginPage(driver)
            .goToCreateNewUserPage()
            .typeEmail(generateEmail)
            .typePswd(wrongPassword)
            .confirmPswd(wrongPassword)
            .submitRegisterWithFailure()
                .assertCorrectErrorMessageIsDisplayed(expMessage);
    }
}
