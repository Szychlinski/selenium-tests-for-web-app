import org.testng.annotations.Test;
import pages.LoginPage;

public class SuccessfulLogin_Test extends SeleniumBaseTest {

    @Test
    public void successfulLoginTest(){
        new LoginPage(driver)
                .loginAs(config.getApplicationUser(), config.getApplicationPassword())
                .assertWelcomeElementIsDisplayed()
                .assertHomePageURL(config.getApplicationUrl());
    }
}
