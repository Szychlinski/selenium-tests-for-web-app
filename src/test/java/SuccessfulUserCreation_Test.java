import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;
import pages.LoginPage;

public class SuccessfulUserCreation_Test extends SeleniumBaseTest {

    @Test
    public void successfulUserCreation(){
        String generateEmail = RandomStringUtils.randomAlphanumeric(10) + "@test.com";
        String newPassword = generateNewPassword(2, 2, 2, 1);

        new LoginPage(driver)
            .goToCreateNewUserPage()
                .assertCorrectRegisterURL(config.getApplicationUrl() + "Account/Register")
            .typeEmail(generateEmail)
            .typePswd(newPassword)
            .confirmPswd(newPassword)
            .submitRegister()
                .assertHomePageURL(config.getApplicationUrl())
                .assertWelcomeElementDisplaysCorrectLogin(generateEmail);
    }
}
