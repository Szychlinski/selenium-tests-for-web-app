import org.apache.commons.lang3.RandomStringUtils;
import org.testng.annotations.Test;
import pages.LoginPage;

public class FailedRegistrationPasswordsDontMatch_Test extends SeleniumBaseTest {

    @Test
    public void failedUserCreationPasswordsDontMatch(){
        String generateEmail = RandomStringUtils.randomAlphanumeric(10) + "@test.com";
        String password1 = generateNewPassword(1, 2, 3, 2);
        String password2 = generateNewPassword(1, 2, 3, 2);

        new LoginPage(driver)
            .goToCreateNewUserPage()
                .assertCorrectRegisterURL(config.getApplicationUrl() + "Account/Register")
            .typeEmail(generateEmail)
            .typePswd(password1)
            .confirmPswd(password2)
            .submitRegisterWithFailure()
                .assertConfirmPasswordErrorIsDisplayed();
    }
}
