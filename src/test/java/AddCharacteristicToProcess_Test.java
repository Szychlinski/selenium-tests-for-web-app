import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.Random;
import java.util.UUID;

public class AddCharacteristicToProcess_Test extends SeleniumBaseTest {

    @Test
    public void addCharacteristic(){
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        int randomInt = new Random().nextInt(10);
        String lsl = Integer.toString(randomInt);
        String usl = Integer.toString(randomInt + 10);
        String histogram = Integer.toString((new Random().nextInt(20))*7);


        new LoginPage(driver)
            .loginAs(config.getApplicationUser(), config.getApplicationPassword())
            .goToCharacteristicsPage()
                .assertCorrectURL(config.getApplicationUrl() + "Characteristics")
                .assertCorrectPageTitle()
                .assertAddCharBtnIsDisplayed()
                .assertAddCharBtnContainsCorrectText()
            .goToCreateCharacteristicPage()
                .assertCorrectPageTitle()
                .assertCorrectURL(config.getApplicationUrl() + "Characteristics/Create")
            .selectProcess(processName)
            .typeCharacteristicName(characteristicName)
            .typeLowerSpecificationLimit(lsl)
            .typeUpperSpecificationLimit(usl)
            .typeHistogramBinCount(histogram)
            .submitCharacteristic()
            .assertCharacteristicSuccessfullyAdded(characteristicName, processName, lsl, usl, histogram);
    }
}
