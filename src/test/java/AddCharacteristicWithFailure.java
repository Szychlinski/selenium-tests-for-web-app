import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.Random;
import java.util.UUID;

public class AddCharacteristicWithFailure extends SeleniumBaseTest {

    @Test
    public void addCharacteristicWithoutUpperSpecLimit(){
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        int randomInt = new Random().nextInt(10);
        String lsl = Integer.toString(randomInt);
        String histogram = Integer.toString((new Random().nextInt(20))*7);


        new LoginPage(driver)
            .loginAs(config.getApplicationUser(), config.getApplicationPassword())
            .goToCharacteristicsPage()
            .goToCreateCharacteristicPage()
            .selectProcess(processName)
            .typeCharacteristicName(characteristicName)
            .typeLowerSpecificationLimit(lsl)
            .typeHistogramBinCount(histogram)
            .submitCharacteristicWithFailure()
                .assertUpperSpecLimitErrorIsDisplayed()
            .backToCharacteristicsPage()
                .assertCharacteristicWasNotAdded(characteristicName);
    }
}
